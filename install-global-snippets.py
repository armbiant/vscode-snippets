#!/usr/bin/env python3
import argparse
from enum import Enum, auto
import json
import os
import traceback

# How many spaces hould be used to indent JSON in the output files?
JSON_INDENT = 4
# Should keys be sorted alphabetically?
JSON_SORT_KEYS = True

# Where are snippets usually stored?
KNOWN_LOCATIONS = [
    "~/.config/Code - OSS/User/snippets/", # Linux
    "~/.config/Code/User/snippets/", #Linux
    "~/Library/Application Support/Code/User/snippets/", # MacOS
]
KNOWN_LOCATIONS = [os.path.expanduser(dir) for dir in KNOWN_LOCATIONS]

class UserError(Exception):
    pass

class MergeStrategy(Enum):
    MERGE_OVERWRITE_OLD = auto()
    MERGE_KEEP_OLD = auto()
    REPLACE = auto()
    SKIP = auto()

def add_snippet_files(snippet_dirs: list[str], snippet_files: list[str], merge_strategy: MergeStrategy):
    # Make sure only unique file names exist
    snippet_names = [os.path.basename(path) for path in snippet_files]
    if len(set(snippet_names)) != len(snippet_names):
        raise UserError("Multiple snippet files have the same name. Please merge them beforehand or call this program multiple times each time only supplying unique file names.")
    
    for dir in snippet_dirs:
        if os.path.isdir(dir):
            print(f"[*] Entering directory {dir}")
            for new_snippet_file in snippet_files:
                name = os.path.basename(new_snippet_file)
                old_snippet_file = os.path.join(dir, name)
                try:
                    add_single_snippet_file(old_snippet_file, new_snippet_file, merge_strategy)
                except Exception as ex:
                    traceback.print_exc()
        else:
            print(f"[*] Directory does not exist: {dir}")


def add_single_snippet_file(old_file: str, new_file: str, merge_strategy: MergeStrategy):
    if os.path.exists(old_file):
        if merge_strategy == MergeStrategy.MERGE_KEEP_OLD:
            print(f"Merging {new_file} into {old_file}")
            merge_files(old_file, new_file, old_file)
        elif merge_strategy == MergeStrategy.MERGE_OVERWRITE_OLD:
            print(f"Merging {new_file} into {old_file}")
            merge_files(new_file, old_file, old_file)
        elif merge_strategy == MergeStrategy.REPLACE:
            print(f"Replacing {old_file} with {new_file}")
            copy_to_and_verify(new_file, old_file)
        elif merge_strategy == MergeStrategy.SKIP:
            print(f"Skipping {new_file}, since {old_file} already exists")
        else:
            raise Exception(f"Unknown merge strategy: {merge_strategy}")
    else:
        print(f"Creating new file: {old_file}")
        copy_to_and_verify(new_file, old_file)

def merge_files(input_high_priority: str, input_low_priority: str, output: str):
    data_high = read_json_file(input_high_priority)
    data_low = read_json_file(input_low_priority)

    # merge dictionaries
    data = data_low
    data.update(data_high)

    write_json_file(output, data)


def copy_to_and_verify(input: str, output: str):
    data = read_json_file(input)
    write_json_file(output, data)

def read_json_file(path: str) -> dict:
    try:
        with open(path, "r") as f:
            result = json.load(f)
    except Exception as ex:
        raise Exception(f"Failed to read JSON file: {path}")
    if type(result) != dict:
        raise Exception(f"Expected to load a dictoriary from {path}, but got a {type(result)}")
    return result

def write_json_file(path: str, object: dict) -> None:
    try:
        with open(path, "w") as f:
            json.dump(object, f, indent=JSON_INDENT, sort_keys=JSON_SORT_KEYS)
    except Exception as ex:
        raise Exception(f"Failed to write JSON file: {path}")


def main():
    ap = argparse.ArgumentParser(description="Adds the given snippets into you global VS code snippet directory")
    group_strategy_pretty = ap.add_argument_group(title="What to do when the file already exists")
    group_strategy = group_strategy_pretty.add_mutually_exclusive_group(required=True)
    group_strategy.add_argument("--merge", action="store_true", help="merge both files (new snippets take precedence)")
    group_strategy.add_argument("--keep",  action="store_true", help="merge both files (old snippets take precedence)")
    group_strategy.add_argument("--replace",  action="store_true", help="only use new snippets")
    group_strategy.add_argument("--skip",  action="store_true", help="do not modify the old snippets")

    ap.add_argument("snippet_files", nargs="+", help="the snippet files to add")
    args = ap.parse_args()

    if args.merge:
        strategy = MergeStrategy.MERGE_OVERWRITE_OLD
    elif args.keep:
        strategy = MergeStrategy.MERGE_KEEP_OLD
    elif args.replace:
        strategy = MergeStrategy.REPLACE
    elif args.skip:
        strategy = MergeStrategy.SKIP
    else:
        raise Exception("Undefined merge strategy")

    add_snippet_files(KNOWN_LOCATIONS, args.snippet_files, strategy)


if __name__ == "__main__":
    main()
