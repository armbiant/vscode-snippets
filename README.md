# My Visual Studio Code Snippets

## Usage

! Work in progress !

Add markdown snippets for various code listings:
```bash
./generator/markdown-code-listings.py > "$HOME/.config/Code - OSS/User/snippets/markdown.json"
```

```bash
mkdir build
./generator/markdown-code-listings.py > build/markdown.json
./install-global-snippets.py --merge build/markdown.json
```

## Planned

A collection of:

- my favourite snippets
- small scripts to generate and manage snippets

