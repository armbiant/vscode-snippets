#!/usr/bin/env python3
# Generate snippets for use with https://github.com/six-two/mkdocs-placeholder-plugin
# For each defined variable a snippet should be defined (basically like an autocomplete for variables, so that you do not constantly need to check the placeholder-plugin.yaml file)
# It probably makes sense to use this as project scoped snippets (stored in <project_root>/.vscode/python.json) 
import argparse
import json
# pip install pyyaml
import yaml

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("placeholder_file", help="the placeholder-plugin.yaml file to read the placeholder names from")
    args = ap.parse_args()

    with open(args.placeholder_file, "rb") as f:
        placeholder_data = yaml.safe_load(f)

    snippets = {}
    for name in sorted(placeholder_data):
        name = str(name)
        l_name = name.lower()
        snippets[f"placeholder-{l_name}"] = {
            "prefix": [
                l_name,
                f"placeholder-{l_name}",
            ],
            "body": f"x{name}x",
            "description": f"Placeholder: x{name}x",
        }

    print(json.dumps(snippets, indent=4))


if __name__ == "__main__":
    main()
