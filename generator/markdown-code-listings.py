#!/usr/bin/env python3
import json

LANGUAGE_LIST = ["javascript", "python", "markdown", "bash", "fish", "powershell"]
snippets = {}

for language in LANGUAGE_LIST:
    snippets[f"code-listing-{language}"] = {
        "prefix": [
            language,
            f"snippet-{language}",
            f"code-listing-{language}",
        ],
        "body": [
            f"```{language}",
            "${1:${TM_SELECTED_TEXT:TODO}}",
            "```",
            ""
        ],
        "description": f"Add a markdown code block with {language} syntax highlighting",
    }

print(json.dumps(snippets, indent=4))
